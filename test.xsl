<?xml version="1.0"?>
<files>
	<file filename="klaviyo_custom_products_lv.xml">
		<xsl:stylesheet version="1.0"
			xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
			xmlns:php="http://php.net/xsl" exclude-result-prefixes="php">
			<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
			<xsl:template match="/">
				<!-- IMPORTANT: ADJUST THIS! Use your currency, three letter code -->
				<xsl:variable name="currency">
					<xsl:text>EUR</xsl:text>
				</xsl:variable>
				<rss version="2.0"
					xmlns:g="http://base.google.com/ns/1.0">
					<channel>
						<title>Klaviyo Custom Products LV</title>
						<link>https://sportland.lv/</link>
						<description>Klaviyo Custom Products LV</description>
						<xsl:for-each select="objects/object">
							<xsl:element name="item">
								<xsl:element name="id">
									<xsl:value-of select="concat(sku, '-lv')"/>
								</xsl:element>
								<xsl:element name="title">
									<xsl:value-of select="name"/>
								</xsl:element>
								<xsl:element name="description">
									<xsl:value-of select="cats/cat/meta_description"/>
								</xsl:element>
								<xsl:element name="categories">
								    <xsl:text>lv</xsl:text>
								</xsl:element>
								<xsl:element name="gender">
									<xsl:value-of select="gender"/>
								</xsl:element>
								<xsl:element name="activity">
									<xsl:value-of select="activity"/>
								</xsl:element>
								<xsl:element name="link">
									<xsl:choose>
										<xsl:when test="parent_item/product_url">
											<xsl:text disable-output-escaping="yes">https://sportland.lv/product/</xsl:text>
											<xsl:value-of select="parent_item/url_key"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text disable-output-escaping="yes">https://sportland.lv/product/</xsl:text>
											<xsl:value-of select="url_key"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:element>
								<xsl:element name="image_link">
									<xsl:choose>
										<xsl:when test="images/image/url">
											<xsl:value-of select="images/image/url"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="image" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:element>
								<xsl:element name="activity">
									<xsl:value-of select="activity"/>
								</xsl:element>
								<xsl:element name="brand">
									<xsl:value-of select="brand"/>
								</xsl:element>
								<xsl:element name="season">
									<xsl:value-of select="season"/>
								</xsl:element>
								<xsl:element name="accessories_size">
									<xsl:value-of select="accessories_size"/>
								</xsl:element>
								<xsl:variable name="original_price" select="number(child_products/child_product/original_price)" />
								<xsl:variable name="special_price" select="number(child_products/child_product/special_price)" />
								<xsl:element name="price">
									<xsl:value-of select="php:functionString('number_format', $original_price, 2, '.', '')"/>
								</xsl:element>
								<xsl:choose>
									<xsl:when test="$special_price != $original_price">
										<xsl:element name="sale_price">
											<xsl:value-of select="php:functionString('number_format', $special_price, 2, '.', '')"/>
										</xsl:element>
									</xsl:when>
									<xsl:otherwise>
										<xsl:element name="sale_price"></xsl:element>
									</xsl:otherwise>
								</xsl:choose>
								<!--<xsl:choose>-->
									<xsl:if test="$special_price != $original_price">
										<xsl:element name="discount">
											<xsl:variable name="discount" select="round(($original_price - $special_price) * 100 div $original_price)" />
											<xsl:value-of select="$discount"/>
										</xsl:element>
									</xsl:if>
							</xsl:element>
						</xsl:for-each>
						<xsl:for-each select="objects/object">
							<xsl:element name="item">
								<xsl:element name="id">
									<xsl:value-of select="concat(sku, '-lv')"/>
								</xsl:element>
								<xsl:element name="title">
									<xsl:value-of select="name"/>
								</xsl:element>
								<xsl:element name="description">
									<xsl:value-of select="cats/cat/meta_description"/>
								</xsl:element>
								<xsl:element name="categories">
								    <xsl:text>lv</xsl:text>
								</xsl:element>
								<xsl:element name="gender">
									<xsl:value-of select="gender"/>
								</xsl:element>
								<xsl:element name="activity">
									<xsl:value-of select="activity"/>
								</xsl:element>
								<xsl:element name="link">
									<xsl:choose>
										<xsl:when test="parent_item/product_url">
											<xsl:text disable-output-escaping="yes">https://sportland.lv/product/</xsl:text>
											<xsl:value-of select="parent_item/url_key"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text disable-output-escaping="yes">https://sportland.lv/product/</xsl:text>
											<xsl:value-of select="url_key"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:element>
								<xsl:element name="image_link">
									<xsl:choose>
										<xsl:when test="images/image/url">
											<xsl:value-of select="images/image/url"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="image" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:element>
								<xsl:element name="activity">
									<xsl:value-of select="activity"/>
								</xsl:element>
								<xsl:element name="brand">
									<xsl:value-of select="brand"/>
								</xsl:element>
								<xsl:element name="season">
									<xsl:value-of select="season"/>
								</xsl:element>
								<xsl:element name="accessories_size">
									<xsl:value-of select="accessories_size"/>
								</xsl:element>
								<xsl:variable name="original_price" select="number(child_products/child_product/original_price)" />
								<xsl:variable name="special_price" select="number(child_products/child_product/special_price)" />
								<xsl:element name="price">
									<xsl:value-of select="php:functionString('number_format', $original_price, 2, '.', '')"/>
								</xsl:element>
								<xsl:choose>
									<xsl:when test="$special_price != $original_price">
										<xsl:element name="sale_price">
											<xsl:value-of select="php:functionString('number_format', $special_price, 2, '.', '')"/>
										</xsl:element>
									</xsl:when>
									<xsl:otherwise>
										<xsl:element name="sale_price"></xsl:element>
									</xsl:otherwise>
								</xsl:choose>
								<!--<xsl:choose>-->
									<xsl:if test="$special_price != $original_price">
										<xsl:element name="discount">
											<xsl:variable name="discount" select="round(($original_price - $special_price) * 100 div $original_price)" />
											<xsl:value-of select="$discount"/>
										</xsl:element>
									</xsl:if>
							</xsl:element>
						</xsl:for-each>
					</channel>
				</rss>
			</xsl:template>
		</xsl:stylesheet>
	</file>
</files>